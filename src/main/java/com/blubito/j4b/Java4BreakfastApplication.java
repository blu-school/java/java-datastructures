package com.blubito.j4b;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Java4BreakfastApplication {

    public static void main(String[] args) {
        SpringApplication.run(Java4BreakfastApplication.class, args);
    }

}
