package com.blubito.j4b.datastructures;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

public class ComputationalTimeComplexity {

	static volatile int value;

	public static void main(String... args) {

		for (int i = 0; i < 3; i++) {
			testAll(i);
		}
	}

	public static void constantTime(Stack<Integer> stack,int r, int n){
		long time = System.currentTimeMillis();
		for (int i = 0; i < r; i++) {
			stack.pop();
		}
		time = System.currentTimeMillis() - time;
		System.out.println(n + " time = " + time);
	}

	private static void testAll(int round) {
		// constant time O(1) e.g. hashing
		System.out.printf("constantTime round %d:%n", round);
		for (int n = 100; n <= 1_600 && n > 0; n *= 2) {
			Stack<Integer> stack = new Stack<>();
			for (int i = 0; i < n; i++) {
				stack.push(i);
			}
			constantTime(stack,5,n);
		}
		value = 42;
		// linear time O(n) e.g. searching a list
		System.out.printf("linearComplexity round %d:%n", round);
		for (int n = 100_000_000; n <= 1_600_000_000 && n > 0; n *= 2) {
			linearComplexity(n);
		}
		// quadratic time O(n*n) e.g. bubble sort
		System.out.printf("quadraticComplexity round %d:%n", round);
		for (int n = 1_000; n <= 100_000 && n > 0; n *= 2) {
			quadraticComplexity(n);
		}
		// logarithmic time O(log n) e.g. tree search
		System.out.printf("logarithmicComplexity round %d:%n", round);
		for (long n = 1_000_000_000L; n <= 1_000_000_000_000L && n > 0; n *= 2) {
			logarithmicComplexity(n);
		}

		// quasilinear time O(n * log n) e.g. merge sort
	}

	private static void linearComplexity(int n) {
		long time = System.currentTimeMillis();
		for (int i = 0; i < n; i++) {
			value = i;
		}
		time = System.currentTimeMillis() - time;
		System.out.println(n + " time = " + time);
	}

	private static void quadraticComplexity(int n) {
		long time = System.currentTimeMillis();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				value = j;
			}
		}
		time = System.currentTimeMillis() - time;
		System.out.println(n + " time = " + time);
	}
	private static void logarithmicComplexity(long n) {
		int steps = 0;
		long time = System.currentTimeMillis();
		for (long i = 1; i < n; i *= 2) {
			value = (int) i;
			steps++;
		}
		time = System.currentTimeMillis() - time;
		System.out.println(n + " time = " + time + " steps = " + steps);
	}
}

