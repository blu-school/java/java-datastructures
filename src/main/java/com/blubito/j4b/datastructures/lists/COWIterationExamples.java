package com.blubito.j4b.datastructures.lists;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class COWIterationExamples
{
   public static void main( String... args )
   {
      List<String> names = new CopyOnWriteArrayList<>( Arrays.asList( "John", "Anton", "Dimitar" ) );
      //sortList(names);

      //noinspection ForLoopReplaceableByForEach
      for( Iterator<String> it = names.iterator(); it.hasNext(); ) {
         String name = it.next();
         System.out.println( "Checking: " + name );
         if( name.contains( "o" ) ) {
            names.remove( name );
         }
      }

      /*
      for( final String name : names ) {
         if( name.contains( "o" ) ) {
            names.remove( name );
         }
      }
      */
      System.out.println( "names = " + names );
   }

   private static void sortList( List<String> names )
   {
      names.sort( null );
      //Collections.sort( names );
      System.out.println( "names = " + names );
      //final List<String> collect = names.stream().sorted().collect( Collectors.toList() );
      //System.out.println( "collected = " + collect );
   }
}
