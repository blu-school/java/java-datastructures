package com.blubito.j4b.datastructures.lists;

import java.util.*;

public class IterationExamples
{
   public static void main( String... args )
   {
      List<String> names = new ArrayList<>(
         Arrays.asList( "John", "Anton", "Dimitar" ) );

      iterateEnumeration(names);

//      iterateList(names);

//      loopList( names );

//      names.listIterator();

   }

   private static void iterateEnumeration( List<String> orig )
   {
      Vector<String> names = new Vector<>( orig );
      Enumeration<String> en = names.elements();

      while( en.hasMoreElements() ) {
         final String name = en.nextElement();
         if( name.contains( "o" ) ) {
            names.remove( name );
         }
         System.out.println( name );
      }

      System.out.println( "names = " + names );
   }

   private static void iterateList( List<String> orig )
   {
      List<String> names = orig;
      Iterator<String> it = names.iterator();

      while( it.hasNext() ) {
         final String name = it.next();
         if( name.contains( "o" ) ) {
            names.remove( name );
         }
         System.out.println( name );
      }

      System.out.println( "names = " + names );
   }

   private static void loopList( List<String> orig )
   {
      List<String> names = orig;

      for( String name : names ) {
         if( name.contains( "o" ) ) {
            names.remove( name );
         }
      }

      for( int i = 0; i < names.size(); i++ ) {
         final String name = names.get( i );
         if( name.contains( "o" ) ) {
            names.remove( name );
         }
      }

      names.forEach( name -> {
         if( name.contains( "o" ) ) {
            names.remove( name );
         }
      } );
      System.out.println( "names = " + names );

   }

}
