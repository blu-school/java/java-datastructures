package com.blubito.j4b.datastructures.lists;

import java.util.LinkedList;

public class LinkedListExamples
{
   private final static class MyLinkedList<E>
   {
      private final class Node<E>
      { // 12
         private Node<E> previous, next; // 4 + 4
         private E e; // 4 ≈ 24 bytes

         public Node( E e )
         {
            this.e = e;
         }
      }

      private Node<E> head;
      private Node<E> tail;

      public void add( E e )
      {
         if( head == null ) {
            head = tail = new Node<>( e );
         }
         else {
            Node<E> node = new Node<>( e );
            node.previous = tail;
            tail.next = node;
            tail = node;
         }
      }
   }

   public static void main( String... args )
   {
      LinkedList<String> queue = new LinkedList<>(); // almost never use this class

      queue.add( "John" );
      queue.add( "Anton" );
      queue.add( "Dimitar" );
      queue.add( "Nick" );
      queue.add( "Layla" );
      queue.add( "Georgi" );

      final String removed = queue.remove( 3 );
      //this will iterate until 4th element and unlink it ... so it can be relative expensive operation

   }
}
