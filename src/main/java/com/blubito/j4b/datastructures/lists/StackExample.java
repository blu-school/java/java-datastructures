package com.blubito.j4b.datastructures.lists;

import java.util.*;

public class StackExample {
  public static void main(String... args) {
    Stack<String> stack = new Stack<>(); // better to use ConcurrentLinkedDeque
    stack.push("Tomatoes");
    stack.push("Carrots");
    stack.push("Cucumbers");
    while(!stack.isEmpty()) {
      System.out.println(stack.pop());
    }

  }
}
