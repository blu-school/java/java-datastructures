package com.blubito.j4b.datastructures.lists;

import java.util.*;

public class VectorExamples {
  public static void main(String... args) {
    Vector<String> names = new Vector<>(
        Arrays.asList("John", "Anton", "Dimitar") // at this point the size of array internally is 3 with 3 elements
    );
    names.add( "Nick" ); // at this point the size of array is 6 ... with 4 elements
    names.add( "Jan");
    names.add( "Layla");
    names.add( "Georgi"); // at this point the size of array is 12 ... with 7 elements
    // vector grows fast when extending , it always doubles previous size when max have been reached

    // when you need to use synchronized list ... use this construct
    List<String> namesSafe = Collections.synchronizedList(
        new ArrayList<>(Arrays.asList("John", "Anton", "Dimitar")));
  }
}
