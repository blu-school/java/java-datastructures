package com.blubito.j4b;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
class Java4BreakfastApplicationTests {

    public interface Callable {

        String soSomethig(String text);
    }

    @Mock
    Callable callable;

    @Test
    void contextLoads() {
        //prepare
        when(callable.soSomethig("test")).thenReturn("tset");
        when(callable.soSomethig(anyString())).thenReturn("tset");

    }

}
